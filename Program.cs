﻿using System.Net;
using System.Text;

namespace HttpServer 
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //main variables
            using var listener = new HttpListener();
            string urlAndPort = "http://localhost:8001/";
            listener.Prefixes.Add("http://*:8001/");
            bool ServerRunning = true;
            int numericValueTest;

            //redirect variables
            bool CustomRedirect = false;
            string redirectUrl = String.Empty;


            listener.Start();

            Console.WriteLine("Listening on port 8001...");
            

            //main server loop
            while (ServerRunning)
            {
                //get context + request + response
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest req = context.Request;

                Console.WriteLine($"Received request for {req.Url}");

                using HttpListenerResponse resp = context.Response;


                //Decode url and make responses
                string data = DecodeUrl(req.Url, urlAndPort);

                //if response is status code
                
                if (int.TryParse(data, out numericValueTest))
                {
                    resp.StatusCode = numericValueTest;
                }

                //add redirect if needed
                if (CustomRedirect)
                {
                    resp.Redirect(redirectUrl);
                    CustomRedirect = false;
                }

                //send data to client
                byte[] buffer = Encoding.UTF8.GetBytes(data);
                resp.ContentLength64 = buffer.Length;

                using Stream ros = resp.OutputStream;
                ros.Write(buffer, 0, buffer.Length);

                
            }

            string GenerateStringResponse(string[] subs, Uri url)
            {
                String response = String.Empty;

                switch (subs[0])
                {
                    case "redirect":
                        if(subs.Length <= 2)
                        {
                            response = "301";
                            CustomRedirect = true;

                            redirectUrl = url.ToString();
                            redirectUrl = redirectUrl.Replace("/redirect/", "/status/401/");
                        }
                        else
                        {
                            if (subs[1] == "infinite")
                            {
                                response = "301";
                                //set newlocation header
                                CustomRedirect = true;

                                redirectUrl = url.ToString();
                            }
                        }
                        break;
                    case "body":
                        if (subs.Length < 1)
                        {
                            response = "BODY EXPECTS TO HAVE /location";
                        }
                        else
                        {
                            if (subs[1] == "json")
                            {
                                //return json
                                response = "{\"Id\":\"123\",\"DateOfRegistration\":\"2012-10-21T00:00:00+05:30\",\"Status\":0}";

                                //set new header
                                redirectUrl = url.ToString();
                                redirectUrl = redirectUrl.Replace("body","application");
                                CustomRedirect = true;
                                break;
                            }

                            if (subs[1] == "html")
                            {
                                //return html
                                response = "<p>Test</p>";

                                //set new header
                                redirectUrl = url.ToString();
                                redirectUrl = redirectUrl.Replace("body","text");
                                CustomRedirect = true;
                            }

                            return "Code ERROR";
                        }
                        break;
                    default:
                        // error
                        response = "CODE ERROR";
                        break;
                }
                
                return response;
            }

           String DecodeUrl(Uri url, string urlandport)
            {
                String response = String.Empty;
                int numericValue;
                bool isNumber = false;

                //url to string
                string editedUrl = url.ToString();

                //remove base of url
                editedUrl = editedUrl.Replace(urlandport, "") ;

                //split edited url by "/"
                string[] subs = editedUrl.Split('/');

                if (subs.Length >1)
                {
                    //check if url end in number
                    isNumber = int.TryParse(subs[1], out numericValue);
                }

                if(isNumber)
                {
                    //respond with number if request is based on number
                    //tukaj nisem prepričan če je response pravi!!!
                    return response = subs[1];
                }else
                {
                    //generate custom response and redirect if request does not end in number
                    return GenerateStringResponse(subs, url);
                }

            }
        }
    }
}